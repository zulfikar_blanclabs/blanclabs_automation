package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.BaseClass;

import java.util.List;

public class Agent_pageObject extends BaseClass {


    public static Logger log = LogManager.getLogger(Agent_pageObject.class);

    @FindBy(id = "username")
    public WebElement username;

    @FindBy(id = "password")
    public WebElement userPassword;

    @FindBy(id = "kc-login")
    public WebElement singInButton;





    public Agent_pageObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }



    public void login(String userid, String password) {

        try {
            waitForVisibility(username,10);
            username.sendKeys(userid);
            log.info("entering user id ....");
            userPassword.sendKeys(password);
            log.info("Entering password ....");


        } catch (Exception e) {
            e.printStackTrace();
            log.error("failed due to :::" + e.getMessage());
            Assert.fail(e.getMessage());
        }


    }

    public void signInMethod() {

        try {
            waitForVisibility(singInButton,5);
            singInButton.click();
            log.info("Clicking on signing button ....");


        } catch (Exception e) {
            e.printStackTrace();
            log.error("failed due to :::" + e.getMessage());
            Assert.fail(e.getMessage());
        }


    }

}