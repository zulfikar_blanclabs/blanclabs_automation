Feature: Agent functionality

  @login
  Scenario Outline: agent login functionality

    Given User launch the URL '<URL>' successfully
    And user enter username '<userid>' and password '<password>'
    And click on login button


    Examples:
      | URL       |userid|password|
      | Dev_Agent |agent_user|agent_pass|

